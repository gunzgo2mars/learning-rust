use std::env;

pub fn run() {

    let args: Vec<String> = env::args().collect();
    let command = args[1].clone();

    //println!("Command :> {:?}" , command);

    
    if command == "rocket" {

        println!("Elon AI :> {:?}" , ("Falcon 9" , "Falcon Heavy" , "Starship"));

    } else if command == "cost" {

        println!("Elon AI :> {:?}", (10000 , 20000 , 30000));

    } else {

        println!("Elon AI :> Just ask me about a spacex rockets.");
        
    }

} 