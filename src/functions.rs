
// Functions - Used to store blocks of code for re-use

pub fn run() {

    rocket_info("Starship" , 10000);


    // Bind function values to variables
    let get_sum = add(255 , 255);
    println!("Get sum > {}", get_sum);

    // Closure
    let n3:i32 = 83;
    let add_nums = |n1: i32, n2: i32| n1 + n2 + n3;
    println!("Add Nums > {}" , add_nums(234 , 234));

}

fn rocket_info(name: &str , cost:i32) {

    println!("Rocket > {} < | COST : {}" , name , cost);

}

fn add(n1: i32 , n2: i32) -> i32 {

    n1 + n2

}