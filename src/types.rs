/*

PRIMITIVE TYPES --> 
    Integers : u8 , i8 , u16 , i16 , u32 , i32 , u64 , i64 , u128 , i128 ( Number of bits they take in memory )
    Floats : f32 , f64
    Boolean : (bool)
    Characters : (char)
    Tuples
    Arrays

*/

pub fn run() {


    // Default is i32
    let x = 16;
    println!("Default i32 : {}" , x);

    // Default is f64
    let y = 2.5;
    println!("Default f64 : {}" , y);

    // Add explicit type
    let z: i64 = 239845792873;
    println!("Add Explicit type i64 : {}" , z);

    // Find max size
    println!("Max i32 > {}" , std::i32::MAX);
    println!("Max i64 > {}" , std::i64::MAX);

    // Boolean
    let is_active: bool = true;
    println!("Is Active > {}" , is_active);

    // Get boolean from expression
    let is_greater: bool = 10 < 5;
    println!("Is greater > {}" , is_greater);

    // Char
    let a1 = 'a';
    println!("Char > {}" , a1);

    // Unicode
    let face = '\u{1F600}';
    println!("Face > {}" , face);


    // ALL RESULT >

    println!("{:?}" , (x , y , z , is_active , is_greater , a1 , face));

     
}