// Vectors - Resizable arrays

use std::mem;

pub fn run() {

    let mut numbers: Vec<i32> = vec![1 , 2 , 3 , 4 , 5];
    println!("Array > {:?}" , numbers);

    // Re-assign value
    numbers[2] = 20;
    println!("Re-assign [2] > {:?}" , numbers);

    // Add on to vector
    numbers.push(7);
    numbers.push(8);
    println!("PUSH > {:?}" , numbers);

    // Pop off last value
    numbers.pop();
    println!("POP LAST > {:?}", numbers);

    // Get single value
    println!("Single val > {}" , numbers[0]);

    // Get array length
    println!("Vector length > {}" , numbers.len());

    // Arrays are stack allocated
    println!("Vector occupies {} bytes" , mem::size_of_val(&numbers));

    // Get Slice
    let slice: &[i32] = &numbers[0..2];
    println!("Slice : {:?}" , slice);

    // Loop through vector values
    for x in numbers.iter() {

        println!("Numbers > {}" , x);

    }

    // Loop & mutate values
    for x in numbers.iter_mut() {

        *x *= 2;

    }

    println!("Numbers Vector > {:?}" , numbers);
}