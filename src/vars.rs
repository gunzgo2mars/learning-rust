pub fn run() {

    let name = "Gunz";
    let mut age = 22;

    println!("My name is {} and i am {}" , name , age);
    age = 23;
    println!("My name is {} and i am {}" , name , age);


    // Define constant
    const ID: i32 = 007;
    println!("ID : {}" , ID);

    // Assign multiple vars
    let (rocket_name , rocket_cost) = ("Falcon Heavy" , 10000000);
    println!("Rocket {} : Cost {}" , rocket_name , rocket_cost);

}