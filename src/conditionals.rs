// Conditionals - Used to check the condition of something and act on the result

pub fn run() {

    let cost: u32 = 1000;
    let check_id: bool = false;
    let active_ignition: bool = true;


    // Basic > if || else if || else
    if cost >= 999 && check_id || active_ignition {

        println!("Too much expensive rocket wtf!");

    } else if cost <= 999 && check_id {

        println!("Wow, i want one!");

    } else {

        println!("Don't panic gunz's super astronaut is here!");

    }

    // Shorthand IF
    let is_of_cost = if cost >= 999 { true } else { false };
    println!("Is of cost > {}" , is_of_cost);

}