pub fn run() {

   // Print Console 
    println!("Static fire testing");

    // Basic Formatting
    println!("{} on {}" , "life", "mars");

    // Positional Arguments
    println!("{0} is fucking {2} and {1} or {3}" , "Winner" , "Retard" , "Noob" , "Cyka Bylat");

    // Named Arguments
    println!("{name} is on {rocket}" , name = "Gunz" , rocket = "Starship");

    // Placeholder Traits

    println!("Binary: {:b} , Hex: {:x}, Octal: {:o}" ,10 , 10 , 10 );

    // Placeholder for debug traits
    println!("{:?}" , (10 , true , "Testing debug"));

    // Basic math
    println!("255 + 255 = {}" , 255 + 255);

}