//mod print;
//mod vars;
//mod types;
//mod strings;
//mod tuples;
//mod arrays;
//mod vectors;
//mod conditionals;
//mod loops;
//mod functions;
//mod pointer_ref;
//mod structs;
//mod enums;
mod cli;

fn main() {
    
    /*

        Basic learning for rust
        -> if you want to use function just delete this-> // and type $ cargo run *[don't forget to delete // at mod.]

        -> // = Code
        -> //# = Comments

        Latern-Link > Gunz's space station!

        Ref -> https://www.youtube.com/watch?v=zF34dRivLOw

        
    */

    // #Print
    //print::run();
    
    // #Vars
    //vars::run();

    // #Types
    //types::run();

    // #Strings
    //strings::run();

    // #Tuples
    //tuples::run();

    // #Arrays
    //arrays::run();

    // #Vectors
    //vectors::run();

    // #Conditionals
    //conditionals::run();

    // #Loops
    //loops::run();

    // #Functions
    //functions::run();

    // #Pointer_ref
    //pointer_ref::run();

    // #Structs
    //structs::run();
    
    // #Enums
    //enums::run();

    // #Cli
    cli::run();
    


}