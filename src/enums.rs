// Enums are types which have a few difinite values

enum Movement {
    // Variants
    Up,
    Down,
    Left,
    Right

}

fn move_avatar(m: Movement) {

    // Perform action depending on info

    match m {

        Movement::Up => println!("Avatar move up"),
        Movement::Down => println!("Avatar move down"),
        Movement::Left => println!("Avatar move Left"),
        Movement::Right => println!("Avatar move Right"),

    }

}

pub fn run() {

    let ava1 = Movement::Left;
    let ava2 = Movement::Up;
    let ava3 = Movement::Right;
    let ava4 = Movement::Down;

    move_avatar(ava1);
    move_avatar(ava2);
    move_avatar(ava3);
    move_avatar(ava4);


}