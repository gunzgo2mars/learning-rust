// Structs - Used to create custom data types

// Traditional Struct
/*
struct Colors {
    r: u8,
    g: u8,
    b: u8,
}
*/

// Tuple Struct
struct Colors(u8 , u8 , u8);

struct Rocket {

    rocket_name: String,
    rocket_cost: i32

}

impl Rocket {

    // Constructor rockets
    fn new(name: &str , cost: i32) -> Rocket {

        Rocket {

            rocket_name: name.to_string(),
            rocket_cost: cost

        }

    }
    // Get Full Info
    fn full_info(&self) -> String { 

        format!("{} {}" , self.rocket_name , " There Stick rocket!")

    }
    // Set Rocket Name 
    fn set_rocket_name(&mut self, name: &str) {

        self.rocket_name = name.to_string();

    }
    // Name to tuple
    fn to_tuple(self) -> (String , i32) {

        (self.rocket_name , self.rocket_cost)

    }



}

pub fn run() {

    /* 
    let mut c = Colors {

        r: 255,
        g: 0,
        b: 255

    };

    c.g = 200;

    println!("Colors : {} , {} , {}" , c.r , c.g , c.b);
    */


    let mut c = Colors(255 , 0 , 255);

    c.1 = 200;

    println!("Colors : {} , {} , {}" , c.0 , c.1 , c.2);


    let mut r = Rocket::new("Falcon Heavy" , 10000);
    println!("Rocket cost is {}" , r.rocket_cost);
    println!("Rocket is {}" , r.full_info() );

    r.rocket_cost = 20000;

    println!("Rocket cost with discount is {}" , r.rocket_cost);

    r.set_rocket_name("Starship");

    println!("Rocket is {}" , r.full_info());

    println!("Rocket tuple -> {:?}" , r.to_tuple());

}