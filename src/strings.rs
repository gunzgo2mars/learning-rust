
/*

    PRIMITIVE STR = Immutable fixed-length string somewhere in memory
    STRING = Growble , heap-allocated data structure - Use when you need to modify or own string data


*/

pub fn run() {


    let mut rocket = String::from("Falcon ");

    // Get length
    println!("Length str : {}", rocket.len());

    // Push char
    rocket.push('A');

    // Push string
    rocket.push_str(" and Payload");

    // Capacity in bytes
    println!("Capacity : {}" , rocket.capacity());

    // Check if empty
    println!("Is empty : {}" , rocket.is_empty());

    // Contains
    println!("Contains 'World' {}" , rocket.contains("World"));

    // Replace
    println!("Replace: {}" , rocket.replace("World" , "Test"));

    // Loop through string by whitespace
    for word in rocket.split_whitespace() {
        println!("{}" , word);
    }

    // Create string with capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');
    println!("Create with capacity : {}" , s);

    // Assertion testing
    assert_eq!(2 , s.len());
    assert_eq!(10 , s.capacity());
    println!("{}" , s);

}